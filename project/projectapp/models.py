from django.db import models

APP_CHOICES = ((
    ('Facebook','Facebook'),
    ('LinkedIn','LinkedIn'),
    ('Snapchat','Snapchat'),
))

class User(models.Model):
    app_name = models.CharField(choices=APP_CHOICES, max_length=100)
    points = models.IntegerField()
    screenshot = models.ImageField(upload_to='screenshot',blank=False)
    
class Admin(models.Model):
    app_name =  models.CharField(choices=APP_CHOICES, max_length=100)
    points = models.IntegerField()
    

class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    screenshot = models.ImageField(upload_to='profile_images', default='blank-profile-picture.png')
    appname = models.CharField(max_length=100, blank=True)
    
    def __str__(self):
        return self.username
    
class Points(models.Model):
    appname = models.CharField(max_length=100)
    user = models.CharField(max_length=100)
    points = models.IntegerField()

    def __str__(self):
        return self.user
    
class Post(models.Model):
    user = models.CharField(max_length=100)
    screenshot = models.ImageField(upload_to='post_images')
    caption = models.TextField()
    no_of_points = models.IntegerField(default=0)

    def __str__(self):
        return self.user