from typing import Any
from django.contrib import admin
from django.db.models.fields.related import ForeignKey
from django.db.models.query import QuerySet
from django.forms.models import ModelChoiceField
from django.http.request import HttpRequest
from projectapp.models import User,Admin
from .models import Admin


@admin.register(User)
class AppUserModelAdmin(admin.ModelAdmin):
    list_display = ['app_name', 'points','screenshot']
    
@admin.register(Admin)    
class AdminModelAdmin(admin.ModelAdmin):
    list_display = ['app_name', 'points']
    

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(author=request.user)
    
    def formfield_for_foreignkey(self, db_field,request,**kwargs):
        if db_field.name == "user":
            kwargs['queryset'] = Admin.objects.filter(appname=request.user.username)
        return super().formfield_for_foreignkey(db_field,request,**kwargs)