from rest_framework import serializers
from projectapp.models import User
from projectapp.models import Admin

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model : User
        fields = ['app_name', 'points','screenshot']
        
class AdminSerializer(serializers.ModelSerializer):
    class Meta:
        model : Admin
        fields = ['app_name', 'points','screenshot']