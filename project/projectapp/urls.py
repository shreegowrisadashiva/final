from django.urls import path
from projectapp import views

urlpatterns = [
    path('project/',views.UserView.as_view(),name='project'),
    path('list/',views.UserView.as_view(),name='list'),
    path('project/',views.AdminView.as_view(),name='project'),
    path('list/',views.AdminView.as_view(),name='list'),
    path('', views.home, name='home'),
    path('settings', views.settings, name='settings'),
    path('profile', views.profile, name='profile'),
    path('signup', views.signup, name='signup'),
    path('signin', views.signin, name='signin'),
    path('logout', views.logout, name='logout'),
]
