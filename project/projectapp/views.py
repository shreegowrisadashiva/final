from django.shortcuts import render,redirect
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from rest_framework.response import Response
from projectapp.models import User,Admin
from projectapp.serializers import UserSerializer,AdminSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import permissions
from .models import Profile, Post, Points
from itertools import chain
import random


class UserView(APIView):
    
        
    def post(self,request,format=None):
        serializer = UserSerializer(data=request.data)
        permission_classes = [permissions.AllowAny]
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'App is added successfully','status':'success','candidate':serializer.data},
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors)
    
    
    def get(self,request,format=None):
        candidates = User.objects.all()
        serializer = UserSerializer(candidates,many=True)
        permission_classes = [permissions.AllowAny]
        return Response({'status':'success','candidates':serializer.data},status=status.HTTP_200_OK)


class AdminView(APIView):
    def post(self,request,format=None):
        serializer = AdminSerializer(data=request.data)
        permission_classes = [permissions.IsAdminUser]
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'App is added successfully','status':'success','candidate':serializer.data},
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors)
    
    
    def get(self,request,format=None):
        candidates = Admin.objects.all()
        serializer = AdminSerializer(candidates,many=False)
        permission_classes = [permissions.IsAdminUser]
        return Response({'status':'success','candidates':serializer.data},status=status.HTTP_200_OK)
    
    
    
@login_required(login_url='signin')
def home(request):
    user_object = User.objects.get(username=request.user.username)
    user_profile = Profile.objects.get(user=user_object)
    
    
    user_app_list = []
    feed = []

    user_app = points.objects.filter(point=request.user.username)

    for users in user_app:
        user_app_list.append(users.user)

    for usernames in user_app_list:
        feed_lists = Post.objects.filter(user=usernames)
        feed.append(feed_lists)

    

    # user suggestion starts
    all_users = User.objects.all()
    user_app_all = []

    for user in user_app:
        user_list = User.objects.get(username=user.user)
        user_app_all.append(user_list)
    

    username_profile = []
    username_profile_list = []



    for ids in username_profile:
        profile_lists = Profile.objects.filter(id_user=ids)
        username_profile_list.append(profile_lists)

    


    return render(request, 'home.html', {'user_profile': user_profile, 'apps':user_app_list})




@login_required(login_url='signin')
def points(request):
    username = request.user.username
    app = []

    if app == None:
        
        
        app.save()
        return redirect('/')
    else:
        
        app.save()
        return redirect('/')

@login_required(login_url='signin')
def profile(request, pk):
    user_object = User.objects.get(username=pk)
    user_profile = Profile.objects.get(user=user_object)
    user_points = Post.objects.filter(user=pk)

    points = request.user.username
    user = pk

    context = {
        'user_object': user_object,
        'user_profile': user_profile,
        'user_posts': user_points,
    }
    return render(request, 'profile.html', context)


@login_required(login_url='signin')
def settings(request):
    user_profile = Profile.objects.get(user=request.user)

    if request.method == 'POST':
        
        if request.FILES.get('image') == None:
            image = user_profile.profileimg
            app = request.POST['app']
            points = request.POST['points']

            user_profile.profileimg = image
            user_profile.app = app
            user_profile.points = points
            user_profile.save()
        if request.FILES.get('image') != None:
            image = request.FILES.get('image')
            app = request.POST['app']
            points = request.POST['points']

            user_profile.profileimg = image
            user_profile.app = app
            user_profile.points = points
            user_profile.save()
        
        return redirect('settings')
    return render(request, 'setting.html', {'user_profile': user_profile})

def signup(request):

    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['password2']

        if password == password2:
            if User.objects.filter(email=email).exists():
                messages.info(request, 'Email Taken')
                return redirect('signup')
            elif User.objects.filter(username=username).exists():
                messages.info(request, 'Username Taken')
                return redirect('signup')
            else:
                user = User.objects.create_user(username=username, email=email, password=password)
                user.save()

                #log user in and redirect to settings page
                user_login = auth.authenticate(username=username, password=password)
                auth.login(request, user_login)

                #create a Profile object for the new user
                user_model = User.objects.get(username=username)
                new_profile = Profile.objects.create(user=user_model, id_user=user_model.id)
                new_profile.save()
                return redirect('settings')
        else:
            messages.info(request, 'Password Not Matching')
            return redirect('signup')
        
    else:
        return render(request, 'signup.html')

def signin(request):
    
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            messages.info(request, 'Credentials Invalid')
            return redirect('signin')

    else:
        return render(request, 'signin.html')

@login_required(login_url='signin')
def logout(request):
    auth.logout(request)
    return redirect('signin')



